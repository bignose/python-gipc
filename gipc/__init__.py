# -*- coding: utf-8 -*-
# flake8: noqa
# Copyright 2012-2017 Jan-Philip Gehrcke. See LICENSE file for details.


__version__ = '0.6.0'


from .gipc import pipe, start_process, GIPCError, GIPCClosed, GIPCLocked
